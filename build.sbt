
/// ====
// monorepo config block

import java.io.File

val hgRoot: File = {
	var root = file("").getAbsoluteFile

	while (!(root / ".sbt.bin/scala.conf").exists())
		root = root.getAbsoluteFile.getParentFile.getAbsoluteFile

	root
}

def conf: String => String = {
	import com.typesafe.config.ConfigFactory

	(key: String) =>
		ConfigFactory.parseFile(
			hgRoot / ".sbt.bin/scala.conf"
		).getString(key)
}
// end of monorepo config block

lazy val all: Seq[Def.Setting[_]] =
	Seq(
		scalaVersion := conf("scala.version"),
		scalacOptions ++= conf("scala.options").split("/").toSeq,
		resolvers += Classpaths.typesafeReleases,
		resolvers += Resolver.mavenCentral,
		resolvers += Resolver.jcenterRepo,
		resolvers += "jitpack" at "https://jitpack.io",

		// magic to get templates into the build
		Compile / resourceDirectory := (Compile / scalaSource).value,
		Test / resourceDirectory := (Test / scalaSource).value,
	)

lazy val root = {
	(project in file("."))
		.settings(all: _ *)
}
// end of standard stuff
/// ---


name := "puresand"
organization := "art.peterlavalle"

// read test resources from the test source dir so that i can put resources next to source files
Test / resourceDirectory := (Test / scalaSource).value

libraryDependencies += "commons-codec" % "commons-codec" % conf("commons.codec") // i'm only using this for one thing - see if i can prue it

// i needed tar.gz extraction for spago/psc
libraryDependencies += "org.apache.commons" % "commons-compress" % conf("commons.compress")

// let's me easily scan for files on the classpath - lets me easily find all .purs files
libraryDependencies += "io.github.classgraph" % "classgraph" % "4.8.115"

libraryDependencies += "org.scalatest" %% "scalatest" % conf("scala.test") % Test
