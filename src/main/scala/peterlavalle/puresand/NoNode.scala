package peterlavalle.puresand

import java.io.*
import java.util
import scala.language.postfixOps
import scala.sys.process.{ProcessLogger, *}

trait NoNode {

	def prepare(cmd: String, args: String*): Command

	trait Command extends (File => ProcessLogger => Int) {

		def exec(cwd: File = null): (Int, List[String]) = {
			val lines = new util.LinkedList[String]()
			Command.this.apply(
				cwd
			)(
				ProcessLogger(
					(o: String) => lines.add(s";$o"),
					(e: String) => lines.add(s"!$e")
				)
			) -> {
				(0 until lines.size()).map(lines.get).toList
			}
		}
	}
}

object NoNode {
	def apply(dump: File): NoNode =
		if (dump.getPath != dump.getAbsoluteFile.getPath)
			apply(dump.getAbsoluteFile)
		else
			new NoNode {

				// setup the install
				lazy val spago: File = Extract(dump)

				override def prepare(cmd: String, args: String*): Command = {
					(cwd: File) =>
						val sysPath: String = System.getenv("PATH")
						val newPath: String = spago.getParentFile.getAbsolutePath + File.pathSeparator + sysPath

						val process: ProcessBuilder =
							Process(
								// build the spago command
								command = spago.getAbsolutePath :: cmd :: args.toList,

								// workout where to run
								cwd =
									if (null == cwd)
										dump
									else
										cwd.getAbsoluteFile,

								// prepend the extracted things to the PATH
								"PATH" -> newPath
							)


						(log: ProcessLogger) =>
							process ! log
				}
			}

}
