package peterlavalle.puresand

import peterlavalle.puresand.Mode.{Execute, Multi, Read, Write}

import java.io.File
import scala.annotation.targetName
import scala.language.postfixOps


object Mode {

	def apply(mode: Int): Mode =
		List(Read, Write, Execute)
			.filterNot((m: Mode) => 0 == (mode & m.flag))
			.reduce((_: Mode) | (_: Mode))
}

enum Mode(val flag: Int) derives CanEqual {
	case Read extends Mode(4)
	case Write extends Mode(2)
	case Execute extends Mode(1)
	//	case Multi private(flag: Int) extends Mode(flag)

	private case Multi(of: Set[Mode]) extends Mode(of.map(_.flag).reduce(_ | _))

	def apply(file: File): Unit =
		this match
			case Read => if (!file.canRead) require(file.setReadable(true))
			case Write => if (!file.canWrite) require(file.setWritable(true))
			case Execute => if (!file.canExecute) require(file.setExecutable(true))
			case Multi(of) => of.foreach(_.apply(file))

	@targetName("or")
	def |(them: Mode): Mode =
		if (them == this)
			this
		else
			val l = this match
				case Multi(l) => l
				case _ => Set(this)
			val r = them match
				case Multi(r) => r
				case _ => Set(them)
			Multi(l ++ r)
}
