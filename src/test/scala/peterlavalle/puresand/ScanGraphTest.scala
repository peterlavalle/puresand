package peterlavalle.puresand

import io.github.classgraph.Resource
import org.scalatest.funsuite.AnyFunSuite

class ScanGraphTest extends AnyFunSuite {

	test("hello world") {
		val actual: Set[String] = ScanGraph.map((_: (String, Either[Resource, (Resource, Resource)]))._1).toSet
		val expected: Set[String] = Set(
			"peterlavalle/puresand/FlipBurger",
			"peterlavalle/puresand/page/Fogurt"
		)

		assert(
			expected.forall(actual.contains),
			actual.toList.sorted.foldLeft(
				s"was expecting $expected, but found (${actual.size}):"
			)((_: String) + "\n\t - `" + (_: String) + "`")
		)
	}
}
