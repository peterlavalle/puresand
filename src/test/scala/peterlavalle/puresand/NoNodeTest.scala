package peterlavalle.puresand

import org.scalatest.funsuite.AnyFunSuite

import java.io.File

class NoNodeTest extends AnyFunSuite {
	test("run and check version") {
		TempDir.cache() {
			(temp: File) =>
				assert(
					(0, List(";0.20.3")) == {
						NoNode(temp).prepare("version").exec(cwd = null)
					}
				)
		}
	}
}
